<?php
    use yii\helpers\Url;
    use yii\helpers\Html;
?>

<a class='list-group-item' href="<?= Url::to(['country/view', 'code' => $model->code]); ?>">
    <?= Html::img('@web/images/countries/png100px/'. $model['code'] .'.png');?>
    <?php echo $model->name; ?>

</a>
