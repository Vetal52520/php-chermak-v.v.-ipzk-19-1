<?php
    use yii\helpers\Html;
    use yii\helpers\Url;
?>

<div class="container">
    <div class="row">
        <?php foreach($continents as $continentItem): ?>
            <div class="col-md-4" style="min-height: 550px">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h2>
                            <a href="<?= Url::to(['continent/view', 'code' => $continentItem['code']]); ?>">
                                <?php echo $continentItem['name']; ?>
                            </a>
                        </h2>
                    </div>
                    <div class="panel-body">
                        <?= Html::img('@web/images/continents/'. $continentItem['code'] .'.png', ['width' => '100%']);?>
                        <?php echo $continentItem['description'] ?>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
</div>

