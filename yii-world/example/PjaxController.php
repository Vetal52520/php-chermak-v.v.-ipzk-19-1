<?php


namespace app\controllers;


use app\models\Continent;
use app\models\StudentForm;
use app\models\WorldForm;
use Yii;
use yii\web\Controller;

class PjaxController extends Controller
{
    public function actionGetServerTime()
    {
        return $this->render('get-server-time', ['time' => date("h:i:s")]);
    }

    public function actionCount($counter = 0)
    {
        $counter++;
        return $this->render('count', compact('counter'));
    }

    public function actionGetFullName()
    {
        $model = new StudentForm();
        if (Yii::$app->request->isPjax) {
            $model->load(Yii::$app->request->post());
            $fullName = $model->first_name . " " . $model->last_name;
            return $this->render('get-full-name', compact('model', 'fullName'));
        }
        return $this->render('get-full-name', compact('model'));
    }

    public function actionWorldForm()
    {
        $model = new WorldForm();
        $continents = Continent::find()->asArray()->all();
        $continent = null;

        if (Yii::$app->request->isPjax) {
            $model->load(Yii::$app->request->post());
            $continent = Continent::find()->where(['continent_id' => $model->continent_id])->one();
        }
        return $this->render('world-form', [
            'model' => $model,
            'continents' => $continents,
            'continent' => $continent
        ]);
    }
}